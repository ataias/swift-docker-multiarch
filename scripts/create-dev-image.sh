#!/usr/bin/env bash

SWIFT_VERSION=${SWIFT_VERSION:-5.5.2}
IMAGE=${DEV_IMAGE:-ataias/swift:${SWIFT_VERSION}-focal}

echo "FROM swift:${SWIFT_VERSION}-focal" | \
    docker build -t $IMAGE-amd64 --platform linux/amd64 -

echo "FROM swiftarm/swift:${SWIFT_VERSION}-ubuntu-focal" | \
    docker build -t $IMAGE-arm64 --platform linux/arm64 -

docker push $IMAGE-amd64
docker push $IMAGE-arm64

docker manifest create -a $IMAGE \
    $IMAGE-arm64 \
    $IMAGE-amd64

docker manifest push $IMAGE
