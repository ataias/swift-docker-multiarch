# swift-docker-multi-arch

## Usage

Change the `FROM` in your Dockerfile to either `ataias/swift:5.5.2-focal` or `ataias/swift:5.5.2-focal-slim`.

## Why?

This repo was created as a way to simplify building a [Swift-based Docker image](https://hub.docker.com/_/swift) for
amd64 and arm64. Last I saw (January 1st, 2022), the official Swift image was for amd64, even though the [nightly
repository](https://hub.docker.com/r/swiftlang/swift) has both architectures. Stable arm64 docker images are available
in a community repo: [swiftarm](https://hub.docker.com/r/swiftlang/swift). This repository simply creates images from both.

You can see the [scripts](./scripts) folder for more info on what is done.

## LICENSE

MIT. See [LICENSE file](./LICENSE).
